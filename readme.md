# emojiwall-cbox-adapter

- [cbox.ws](https://www.cbox.ws/) is a chat-embed
- [smol.stream](https://smol.stream) is a hosted emojiwall
- [emojiwall](https://framagit.org/owncast-things/owncast-emojiwall) is the emojiwall implementation of Monsieur Fractal

`cbox.ws` does not expose a public API that could be consumed to allow third-party integration into the chat.
streaming-platforms like [Jnktn.TV](https://jnktn.tv) that utilize `cbox.ws` thus cannot utilize fancy tools like the
[Emojiwall](https://framagit.org/owncast-things/owncast-emojiwall) that require access to chat messages sent by users.

This adapter connects to the `cbox.ws` websocket of your chat embed, extracts all emojis (even custom-emojis)
and forwards them to your emojiwall. You just have to configure the OBS overlay, and you're good to go!

## Running the Adapter

### Installation of dependencies
```bash
pip install -r requirements.tx
```

### Running with Commandline Arguments

```bash
python3 main.py <cbox.ws websocket> <emojiwall url>
```

### Running with Environment Variables

```bash
export EMOJI_SOURCE="<cbox.ws websocket>"
export EMOJI_TARGET="<emojiwall url>"
python3 main.py
```

## Example with smol.stream

```bash
export EMOJI_SOURCE="wss://flr-eu3.cbox.ws:4430/?pool=5-921911-0"
export EMOJI_TARGET="https://beta.smol.stream/emojiwall/webhook/🧈🙊🤱🛰️⚕️👩‍🦯🚊🌡️🪫🐉"
python3 main.py
```

## KUDOS
- [@lefractall@mstdn.social](https://mstdn.social/@lefractal)
