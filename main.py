import re
import os
import sys
import html
import emoji
import asyncio
import requests
import websockets
from loguru import logger


def extract_custom_emotes(msg: str) -> list[str]:
    return [
        '<img src="{0}"/>'.format(r.groups()[0].replace('//cbox.im/', '/'))
        for r in re.finditer('data-url="([0-9A-Za-z/.]*)"', msg)
        if len(r.groups()) == 1
    ]


def extract_unicode_emotes(msg: str) -> list[str]:
    return [e['emoji'] for e in emoji.emoji_list(msg)]


def send_webhook_to_emojiwall(msg: str, emojiwall_url: str) -> None:
    try:
        requests.post(url=emojiwall_url, json={
            'eventData': {
                'body': msg
            }
        })
    except Exception as req_exc:
        logger.warning(req_exc)


def handle_incoming_message(websocket_frame: str, emojiwall_url: str) -> None:
    try:
        if '\t' in websocket_frame and not websocket_frame.startswith('&lt;'):
            _, _, _, user, _, _, msg, _, _, _, _, _ = websocket_frame.split('\t')
            unescaped_msg = html.unescape(msg)
            logger.info("User '{0}': {1}".format(user, unescaped_msg))
            emote_list = extract_unicode_emotes(unescaped_msg) + extract_custom_emotes(unescaped_msg)
            send_webhook_to_emojiwall(msg=' '.join(emote_list), emojiwall_url=emojiwall_url)
    except ValueError:
        logger.warning('Could not parse incoming Websocket Frame "{0}"'.format(websocket_frame))


async def handle_cbox_messages(cbox_websocket_url: str, emojiwall_url: str) -> None:
    while True:
        try:
            async with websockets.connect(cbox_websocket_url) as ws:
                while True:
                    try:
                        websocket_frame = await ws.recv()
                        handle_incoming_message(websocket_frame, emojiwall_url)
                    except asyncio.CancelledError as ce:
                        logger.warning(ce)
                        pass
        except websockets.ConnectionClosedError as cce:
            logger.warning(cce)
            pass

if __name__ == '__main__':
    emoji_source = sys.argv[1] if len(sys.argv) >= 2 else os.environ['EMOJI_SOURCE']
    emoji_target = sys.argv[2] if len(sys.argv) >= 3 else os.environ['EMOJI_TARGET']

    logger.info('Starting Emojiwall Cbox.WS Adapter')
    logger.info('Receiving Emojis from "{0}" as source Cbox'.format(emoji_source))
    logger.info('Sending Emojis to "{0}" as target Emojiwall'.format(emoji_target))

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    try:
        loop.run_until_complete(handle_cbox_messages(emoji_source, emoji_target))
    except KeyboardInterrupt:
        logger.info("Au revoir! Good Bye! Auf Wiedersehen!")
